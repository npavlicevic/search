#include "insert.h"

int *array_create(int size) {
  return (int*)calloc(size, sizeof(int));
}
void array_sort(int *this, int size) {
  int i, j;
  for(i = 1; i < size; i++) {
    for(j = i; j > 0 && (this[j - 1] > this[j]); j--) {
      array_swap(this, j - 1, j);
    }
  }
}
void array_swap(int *this, int a, int b) {
  int temp = this[a];
  this[a] = this[b];
  this[b] = temp;
}
void array_rand(int *this, int size) {
  int max = 128, i = 0;
  for(; i < size; i++) {
    this[i] = rand() % max;
  }
}
void array_print(int *this, int size) {
  int i = 0;
  for(; i < size; i++) {
    printf("%d ", this[i]);
  }
  printf("\n");
}
void array_destroy(int **this) {
  free(*this);
  *this = NULL;
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
