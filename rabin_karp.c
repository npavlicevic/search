#include "rabin_karp.h"

int schedule(char *this, int size, char *pattern, int _size) {
  int i = 0, hash, _hash = array_hash(pattern, _size), j;
  for(; i < size - _size; i++) {
    hash = array_hash(this + i, _size);
    if(hash == _hash) {
      for(j = i; j < i + _size; j++) {
        if(this[j] != pattern[j - i]) {
          return -1;
        }
      }
      return i;
    }
  }
  return -1;
}
char *array_create(int size) {
  return (char*)calloc(size, sizeof(char));
}
void array_rand(char *this, int size) {
  int max = 128, i = 0;
  for(; i < size; i++) {
    this[i] = rand() % max;
  }
}
int array_hash(char *this, int size) {
  int mult = 128, hash = 0, i = 0;
  for(; i < size; i++) {
    hash = mult*hash + this[i];
  }
  return hash;
}
void array_destroy(char **this) {
  free(*this);
  *this = NULL;
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
