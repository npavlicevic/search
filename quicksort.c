#include "quicksort.h"

int *array_create(int size) {
  return (int*)calloc(size, sizeof(int));
}
void array_swap(int *this, int one, int two) {
  int temp = this[one];
  this[one] = this[two];
  this[two] = temp;
}
int array_partition(int *this, int lo, int hi) {
  int pivot = (lo + hi) / 2;
  int pivot_value = this[pivot];
  array_swap(this, pivot, hi);
  int store_index = lo, i = lo;
  for(; i < hi; i++) {
    if(this[i] <= pivot_value) {
      array_swap(this, store_index, i);
      store_index++;
    }
  }
  array_swap(this, store_index, hi);
  return store_index;
}
void array_sort(int *this, int lo, int hi) {
  if(lo < hi) {
    int pivot = array_partition(this, lo, hi);
    array_sort(this, lo, pivot - 1);
    array_sort(this, pivot + 1, hi);
  }
}
void array_rand(int *this, int size) {
  int max = 128, i = 0;
  for(; i < size; i++) {
    this[i] = rand() % max;
  }
}
void array_print(int *this, int size) {
  int i = 0;
  for(; i < size; i++) {
    printf("%d ", this[i]);
  }
  printf("\n");
}
void array_destroy(int **this) {
  free(*this);
  *this = NULL;
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
