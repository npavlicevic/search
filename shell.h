#ifndef SHELL_H
#define SHELL_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
int *array_create(int size);
void array_sort(int *this, int size, int *gaps, int _size);
void array_rand(int *this, int size);
void array_print(int *this, int size);
void array_destroy(int **this);
void seed();

// shell sort
// sort with gaps
// gaps are constant
// start at 1 smallest gap
// worst case o(n^2)
// best case o(n log^2 n)
#endif
