#include "rabin_karp_test.h"

void schedule_test() {
  int txt_size = 8, pat_size = 4, at;
  char *txt = array_create(txt_size), *pat = array_create(pat_size);
  txt[0] = 't';
  txt[1] = 'h';
  txt[2] = 'e';
  txt[3] = 't';
  txt[4] = 'h';
  txt[5] = 'i';
  txt[6] = 'n';
  txt[7] = 'g';
  pat[0] = 't';
  pat[1] = 'h';
  pat[2] = 'i';
  pat[3] = 'n';
  at = schedule(txt, txt_size, pat, pat_size);
  printf("%d\n", at);
  array_destroy(&txt);
  array_destroy(&pat);
}
