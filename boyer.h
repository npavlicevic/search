#ifndef BOYER_H
#define BOYER_H
#include <stdlib.h>
#include <stdio.h>

int schedule(char *this, int size, char *pattern, int _size, int *position);
char *array_create(int size);
void array_char_position(int *this, char *pattern, int size);
void array_destroy();
int *array_int_create(int size);
void array_int_destroy(int **this);

// boyer moore
// string match
// char position
// build table
// table is simple
// table records position of character
#endif
