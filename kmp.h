#ifndef KMP_H
#define KMP_H
#include <stdlib.h>
#include <stdio.h>

int schedule(char *this, int size, char *pattern, char *table, int _size);
char *array_create(int size);
void array_build(char *this, char *pattern, int size);
void array_destroy(char **this);

// kmp knut morris pratt string search
// build table
// table length of pattern
// search with table
// compare pattern with self
#endif
