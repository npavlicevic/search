#include "insert_test.h"

void array_sort_test() {
  int size = 1024;
  int *arr = array_create(size);
  array_rand(arr, size);
  array_sort(arr, size);
  assert(arr[0] <= arr[size - 1]);
  array_print(arr, size);
  array_destroy(&arr);
}
