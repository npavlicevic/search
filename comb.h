#ifndef COMB_H
#define COMB_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
int *array_create(int size);
void array_sort(int *this, int size);
void array_swap(int *this, int a, int b);
void array_rand(int *this, int size);
void array_print(int *this, int size);
void array_destroy(int **this);
void seed();
// comb sort
// uses gaps
// on bubble sort
// worst o(n^2)
// best o(n)
// average o(n^2 / 2^p)
#endif
