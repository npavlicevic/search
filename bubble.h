#ifndef BUBBLE_H
#define BUBBLE_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
int *array_create(int size);
void array_sort(int *this, int size);
void array_swap(int *this, int a, int b);
void array_rand(int *this, int size);
void array_print(int *this, int size);
void array_destroy(int **this);
void seed();

// bubble sort
// keep looping
// O(n^2)
#endif
