#include "boyer.h"

int schedule(char *this, int size, char *pattern, int _size, int *position) {
  int i = 0, j;
  for(; i < (size - _size);) {
    j = _size - 1;
    // start with last char
    while(pattern[j] == this[i + j]) {
      j--;
      if(j < 0) return i;
    }
    if(j < position[(int)this[i + j]]) {
      i++;
      // bad character move forward
    } else {
      i = i + j - position[(int)this[i + j]];
      // bad char + looking glass heuristic
      // if the character is not in the pattern move past in text
    }
  }
  return -1;
}
char *array_create(int size) {
  return (char*)calloc(size, sizeof(char));
}
void array_char_position(int *this, char *pattern, int size) {
  int i = 0;
  for(; i < size; i++) {
    this[(int)pattern[i]] = i;
  }
}
void array_destroy(char **this) {
  free(*this);
  *this = NULL;
}
int *array_int_create(int size) {
  return (int*)calloc(size, sizeof(int));
}
void array_int_destroy(int **this) {
  free(*this);
  *this = NULL;
}
