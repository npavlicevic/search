#ifndef MERGE_H
#define MERGE_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
int *array_create(int size);
void array_copy(int *this, int *other, int lo, int hi);
void array_merge(int *this, int *other, int lo, int mid, int hi);
void array_split(int *this, int *other, int lo, int hi);
void array_sort(int *this, int *other, int size);
void array_rand(int *this, int size);
void array_print(int *this, int size);
void array_destroy(int **this);
void seed();

// merge sort
// recursive
// lo mid hi
// o(n log n) worst case
#endif
