#ifndef RABIN_KARP_H
#define RABIN_KARP_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
int schedule(char *this, int size, char *pattern, int _size);
char *array_create(int size);
void array_rand(char *this, int size);
int array_hash(char *this, int size);
void array_destroy(char **this);
void seed();

// rabin karp string search
// take hash pattern
// keep taking hash of base string
// compare
#endif
