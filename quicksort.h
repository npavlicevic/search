#ifndef QUICKSORT_H
#define QUICKSORT_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int *array_create(int size);
void array_swap(int *this, int one, int two);
int array_partition(int *this, int lo, int hi);
void array_sort(int *this, int lo, int hi);
void array_rand(int *this, int size);
void array_print(int *this, int size);
void array_destroy(int **this);
void seed();

// quick sort
// recursive
// divide list left right
// around pivot
// worst case o(n^2)
// best case average case o(n log n)
// using recurrence
// t(n) = o(n) + 2t(n/2) 2t 2 rec calls with n / 2 list
// from master o(n log n)
//
// master
// t(n) = at(n/b) + o(n^d)
// a = 2 b = 2 d = 1
// d > log_b a o(n^d)
// d < log_b a o(n^(log_b a))
// d = log_b a o(n ln n)
// d = log_b a = 1 o(n ln n)
#endif
