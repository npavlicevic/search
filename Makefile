#
# Makefile 
# search
#

CC=gcc
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic
LIBS=-lm

FILES=rabin_karp.c rabin_karp_test.c main_rabin_karp.c
FILES_KMP=kmp.c kmp_test.c main_kmp.c
FILES_BOYER=boyer.c boyer_test.c main_boyer.c
FILES_QUICKSORT=quicksort.c quicksort_test.c main_quicksort.c
FILES_SHELL=shell.c shell_test.c main_shell.c
FILES_COMB=comb.c comb_test.c main_comb.c
FILES_MERGE=merge.c merge_test.c main_merge.c
FILES_BUBBLE=bubble.c bubble_test.c main_bubble.c
FILES_INSERT=insert.c insert_test.c main_insert.c
CLEAN=main_rabin_karp main_kmp main_boyer main_quicksort main_shell main_comb main_merge main_bubble main_insert

all: rabin_karp kmp boyer quicksort shell comb merge bubble insert

rabin_karp: ${FILES}
	${CC} ${CFLAGS} $^ -o main_rabin_karp ${LIBS}

kmp: ${FILES_KMP}
	${CC} ${CFLAGS} $^ -o main_kmp ${LIBS}

boyer: ${FILES_BOYER}
	${CC} ${CFLAGS} $^ -o main_boyer ${LIBS}

quicksort: ${FILES_QUICKSORT}
	${CC} ${CFLAGS} $^ -o main_quicksort ${LIBS}

shell: ${FILES_SHELL}
	${CC} ${CFLAGS} $^ -o main_shell ${LIBS}

comb: ${FILES_COMB}
	${CC} ${CFLAGS} $^ -o main_comb ${LIBS}

merge: ${FILES_MERGE}
	${CC} ${CFLAGS} $^ -o main_merge ${LIBS}

bubble: ${FILES_BUBBLE}
	${CC} ${CFLAGS} $^ -o main_bubble ${LIBS}

insert: ${FILES_INSERT}
	${CC} ${CFLAGS} $^ -o main_insert ${LIBS}

clean: ${CLEAN}
	rm $^
