#include "merge.h"

int *array_create(int size) {
  return (int*)calloc(size, sizeof(int));
}
void array_copy(int *this, int *other, int lo, int hi) {
  int i = lo;
  for(; i < hi; i++) {
    this[i] = other[i];
  }
}
void array_merge(int *this, int *other, int lo, int mid, int hi) {
  int i0 = lo, i1 = mid, j;
  for(j = lo; j < hi; j++) {
    if((i0 < mid) && (i1 >= hi || other[i0] <= other[i1])) {
      this[j] = other[i0++];
    } else {
      this[j] = other[i1++];
    }
  }
}
void array_split(int *this, int *other, int lo, int hi) {
  if((hi - lo) < 2) {
    return;
  }
  int mid = (lo + hi) / 2;
  array_split(this, other, lo, mid);
  array_split(this, other, mid, hi);
  array_merge(other, this, lo, mid, hi);
  array_copy(this, other, lo, hi);
}
void array_sort(int *this, int *other, int size) {
  array_split(this, other, 0, size);
}
void array_rand(int *this, int size) {
  int max = 128, i = 0;
  for(; i < size; i++) {
    this[i] = rand() % max;
  }
}
void array_print(int *this, int size) {
  int i = 0;
  for(; i < size; i++) {
    printf("%d ", this[i]);
  }
  printf("\n");
}
void array_destroy(int **this) {
  free(*this);
  *this = NULL;
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
