#include "shell.h"

int *array_create(int size) {
  return (int*)calloc(size, sizeof(int));
}
void array_sort(int *this, int size, int *gaps, int _size) {
  int i, j, k, gap, temp;
  for(i = 0; i < _size; i++) {
    gap = gaps[i];
    for(j = gap; j < size; j++) {
      temp = this[j];
      for(k = j; k >= gap && this[k - gap] > temp; k -= gap) {
        this[k] = this[k - gap];
      }
      this[k] = temp;
    }
  }
}
void array_rand(int *this, int size) {
  int max = 128, i = 0;
  for(; i < size; i++) {
    this[i] = rand() % max;
  }
}
void array_destroy(int **this) {
  free(*this);
  *this = NULL;
}
void array_print(int *this, int size) {
  int i = 0;
  for(; i < size; i++) {
    printf("%d ", this[i]);
  }
  printf("\n");
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
