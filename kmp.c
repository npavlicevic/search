#include "kmp.h"

int schedule(char *this, int size, char *pattern, char *table, int _size) {
  int m = 0, i = 0;
  for(;(m + i) < size;) {
    if(pattern[i] == this[m + i]) {
      if(i == _size - 1) {
        return m;
      }
      i++;
    } else {
      if(table[i] > -1) {
        m = m + i - table[i];
        i = table[i];
      } else {
        i = 0;
        m++;
      }
    }
  }
  return -1;
}
char *array_create(int size) {
  return (char*)calloc(size, sizeof(char));
}
void array_build(char *this, char *pattern, int size) {
  int i = 2, candidate = 0;
  this[0] = -1;
  this[1] = 0;
  for(;i < size;) {
    if(pattern[i - 1] == pattern[candidate]) {
      candidate++;
      this[i] = candidate;
      i++;
    } else if (candidate > 0){
      candidate = this[candidate];
    } else {
      this[i] = 0;
      i++;
    }
  }
}
void array_destroy(char **this) {
  free(*this);
  *this = NULL;
}
