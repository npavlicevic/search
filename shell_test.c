#include "shell_test.h"

void array_sort_test() {
  int size_gaps = 8;
  int *gaps = array_create(size_gaps);
  gaps[0] = 128;
  gaps[1] = 64;
  gaps[2] = 32;
  gaps[3] = 16;
  gaps[4] = 8;
  gaps[5] = 4;
  gaps[6] = 2;
  gaps[7] = 1;
  int size = 1024;
  int *arr = array_create(size);
  array_rand(arr, size);
  array_sort(arr, size, gaps, size_gaps);
  assert(arr[0] <= arr[size - 1]);
  array_print(arr, size);
  array_destroy(&gaps);
  array_destroy(&arr);
}
